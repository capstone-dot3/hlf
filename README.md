## WHEN RESTARTING FABRIC 


So with our current system, the ca requires the user sheridanc to exist and enroll. I'm assuming this is the boostrap identity. Everytime you restart Fabric, it will need you to reinstantiate sheridanc into the system.

STEPS:

1. shutdown/restart fabric
./removeFabric.sh
./startFabric.sh

2. enroll as an admin privilaged user ** ignore the failed to enroll.. it enrolls.. just fix the logic in the code if you're bothered by it
node enrollUser.js admin adminpw

3. delete current sheridanc
fabric-ca-client identity remove sheridanc

4. recreate sheridanc
node addBoostrap.js

5. test queries
./transcript.sh queryAll

6. PROFIT
also, you can enroll as anyone else and it will work too.
Users currently in the system:

admin / adminpw
meggu / meggupw
jacky / jackypw
brian / brianpw
rudi / rudipw
sheridanc / sheridancpw



## Acalink setup of fabAcalink
1. Go to fabacalink folder
## Run the following commands:
2. npm install
3. ./removeFabric
4. ./startFabric
5. node server,js


## Hyperledger Fabric Samples

Please visit the [installation instructions](http://hyperledger-fabric.readthedocs.io/en/latest/install.html)
to ensure you have the correct prerequisites installed. Please use the
version of the documentation that matches the version of the software you
intend to use to ensure alignment.

## Download Binaries and Docker Images

The [`scripts/bootstrap.sh`](https://github.com/hyperledger/fabric-samples/blob/release-1.1/scripts/bootstrap.sh)
script will preload all of the requisite docker
images for Hyperledger Fabric and tag them with the 'latest' tag. Optionally,
specify a version for fabric, fabric-ca and thirdparty images. Default versions
are 1.1.0, 1.1.0 and 0.4.7 respectively.

```bash
./scripts/bootstrap.sh [version] [ca version] [thirdparty_version]
```

## License <a name="license"></a>

Hyperledger Project source code files are made available under the Apache
License, Version 2.0 (Apache-2.0), located in the [LICENSE](LICENSE) file.
Hyperledger Project documentation files are made available under the Creative
Commons Attribution 4.0 International License (CC-BY-4.0), available at http://creativecommons.org/licenses/by/4.0/.
