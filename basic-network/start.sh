#!/bin/bash
#
# Copyright IBM Corp All Rights Reserved
#
# SPDX-License-Identifier: Apache-2.0
#
# Exit on first error, print all commands.
set -ev

# don't rewrite paths for Windows Git Bash users
export MSYS_NO_PATHCONV=1

docker-compose -f docker-compose.yml down

docker-compose -f docker-compose.yml up -d ca.acalink.com orderer.acalink.com peer0.sheridanc.acalink.com peer1.sheridanc.acalink.com couchdb couchdb1

# wait for Hyperledger Fabric to start
# incase of errors when running later commands, issue export FABRIC_START_TIMEOUT=<larger number>
export FABRIC_START_TIMEOUT=10
#echo ${FABRIC_START_TIMEOUT}
sleep ${FABRIC_START_TIMEOUT}

# Create the channel
docker exec -e "CORE_PEER_LOCALMSPID=SheridancMSP" -e "CORE_PEER_MSPCONFIGPATH=/etc/hyperledger/msp/users/Admin@sheridanc.acalink.com/msp" peer0.sheridanc.acalink.com peer channel create -o orderer.acalink.com:7050 -c institutionch -f /etc/hyperledger/configtx/channel.tx
# Join peer0.sheridanc.acalink.com to the channel.
docker exec -e "CORE_PEER_LOCALMSPID=SheridancMSP" -e "CORE_PEER_MSPCONFIGPATH=/etc/hyperledger/msp/users/Admin@sheridanc.acalink.com/msp" peer0.sheridanc.acalink.com peer channel join -b institutionch.block

# "CORE_PEER_LOCALMSPID=Org1MSP" -e 
docker exec -e "CORE_PEER_LOCALMSPID=SheridancMSP" -e "CORE_PEER_MSPCONFIGPATH=/etc/hyperledger/msp/users/Admin@sheridanc.acalink.com/msp" -e "CORE_PEER_ADDRESS=peer1.sheridanc.acalink.com:7051" peer0.sheridanc.acalink.com peer channel join -b institutionch.block