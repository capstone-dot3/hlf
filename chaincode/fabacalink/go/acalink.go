/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

/*
 * The sample smart contract for documentation topic:
 * Writing Your First Blockchain Application
 */

package main

/* Imports
 * 4 utility libraries for formatting, handling bytes, reading and writing JSON, and string manipulation
 * 2 specific Hyperledger Fabric specific libraries for Smart Contracts
 */
import (
	"bytes"
	"encoding/json"
	"fmt"
	"strconv"
	"time"

	"github.com/hyperledger/fabric/core/chaincode/lib/cid"
	"github.com/hyperledger/fabric/core/chaincode/shim"

	sc "github.com/hyperledger/fabric/protos/peer"
)

// Define the Smart Contract structure
type SmartContract struct {
}

// Define the transcript structure, with 4 properties.  Structure tags are used by encoding/json library
type Transcript struct {
	ObjectType  string   `json:"docType"`
	Id          string   `json:"id"`
	StudentID   string   `json:"studentId"`
	Institution string   `json:"institution"`
	Courses     []Course `json:"courses"`
}

// Define the course structure, with 5 prop
type Course struct {
	CourseId       string `json:"courseId"`
	CourseName     string `json:"courseName"`
	Credit         string `json:"credit"`
	Grade          string `json:"grade"`
	Status         string `json:"status"`
	CompletionDate string `json:"completionDate"`
	/*Id   string `json:"id"`		//, CourseId, CourseName, Credit (Float), Grade, S Institution, CompletionDate  */
}

// Define students structure, with 4 prop
type Student struct {
	Id         string `json:"id"`
	FirstName  string `json:"firstName"`
	LastName   string `json:"lastName"`
	MiddleName string `json:"midName"`
}

/*
 * The Init method is called when the Smart Contract "course" is instantiated by the blockchain network
 * Best practice is to have any Ledger initialization in separate function -- see initLedger()
 */
func (s *SmartContract) Init(APIstub shim.ChaincodeStubInterface) sc.Response {
	return shim.Success(nil)
}

/*
 * The Invoke method is called as a result of an application request to run the Smart Contract "course"
 * The calling application program has also specified the particular smart contract function to be called, with arguments
 */
func (s *SmartContract) Invoke(APIstub shim.ChaincodeStubInterface) sc.Response {

	// Retrieve the requested Smart Contract function and arguments
	function, args := APIstub.GetFunctionAndParameters()
	// Route to the appropriate handler function to interact with the ledger appropriately
	if function == "initLedger" {
		return s.initLedger(APIstub)
	} else if function == "createTranscript" {
		return s.createTranscript(APIstub, args)
	} else if function == "addTranscriptCourse" {
		return s.addTranscriptCourse(APIstub, args)
	} else if function == "queryAllTranscripts" {
		return s.queryAllTranscripts(APIstub)
	} else if function == "queryAllTranscriptsByInstitution" {
		return s.queryAllTranscriptsByInstitution(APIstub, args)
	} else if function == "queryTranscriptByStudentID" {
		return s.queryTranscriptByStudentID(APIstub, args)
	} else if function == "queryTranscriptById" {
		return s.queryTranscriptById(APIstub, args)
	} else if function == "deleteTranscript" {
		return s.deleteTranscript(APIstub, args)
	} else if function == "getHistoryForTranscript" {
		return s.getHistoryForTranscript(APIstub, args)
	} else if function == "queryCourse" {
		return s.queryCourse(APIstub, args)
	} else if function == "getAttribute" {
		return s.getAttribute(APIstub)
		// ========== Course functions ==========
	} else if function == "createCourse" {
		return s.createCourse(APIstub, args)
	} else if function == "queryAllCourses" {
		return s.queryAllCourses(APIstub)
	} else if function == "deleteCourse" {
		return s.deleteCourse(APIstub, args)
	} else if function == "queryCourseById" {
		return s.queryCourseById(APIstub, args)
		// ========== Student functions ==========
	} else if function == "queryStudent" {
		return s.queryStudent(APIstub, args)
	} else if function == "createStudent" {
		return s.createStudent(APIstub, args)
	} else if function == "queryAllStudents" {
		return s.queryAllStudents(APIstub)
	} else if function == "deleteStudent" {
		return s.deleteStudent(APIstub, args)
	}

	return shim.Error("Invalid Smart Contract function name.")
}

func (s *SmartContract) initLedger(APIstub shim.ChaincodeStubInterface) sc.Response {
	course := Course{CourseId: "INFO39014", CourseName: "CST Capstone Project", Credit: "3.0", Grade: "86", Status: "Complete", CompletionDate: "Fall2018"}
	course2 := Course{CourseId: "PROG39599", CourseName: "Advance Java Frameworks", Credit: "6.0", Grade: "80", Status: "Complete", CompletionDate: "Fall2017"}

	courses := []Course{course, course2}

	course3 := Course{CourseId: "SYST3379", CourseName: "Wireless Systems", Credit: "6.0", Grade: "90", Status: "SheridanCollege", CompletionDate: "Fall2017"}
	course4 := Course{CourseId: "MATH32668", CourseName: "Statistics - Computer Science", Credit: "6.0", Grade: "85", Status: "SheridanCollege", CompletionDate: "Fall2018"}
	course5 := Course{CourseId: "PROG39402", CourseName: "Advanced Mobile Application Development", Credit: "6.0", Grade: "92", Status: "SheridanCollege", CompletionDate: "Fall2018"}

	courses2 := []Course{course3, course4, course5}

	//SDNE semester 1
	java1 := Course{CourseId: "PROG10082", CourseName: "Object Oriented Programming 1 – Java", Credit: "6.0", Grade: "70", Status: "Complete", CompletionDate: "Winter2017"}
	webdev1 := Course{CourseId: "SYST10049", CourseName: "Web Development", Credit: "3.0", Grade: "56", Status: "Complete", CompletionDate: "Winter2017"}
	tele1 := Course{CourseId: "TELE13167", CourseName: "Introduction to Data Communications and Networking", Credit: "3.0", Grade: "76", Status: "Complete", CompletionDate: "Winter2017"}
	comm1 := Course{CourseId: "COMM13729", CourseName: "The Art of Technical Communication", Credit: "3.0", Grade: "92", Status: "Complete", CompletionDate: "Winter2017"}
	math1 := Course{CourseId: "MATH18584", CourseName: "Computer Math Fundamentals", Credit: "4.0", Grade: "81", Status: "Complete", CompletionDate: "Winter2017"}
	cult := Course{CourseId: "CULT10001G", CourseName: "The Impact of Culture on the Canadian Workplace (mandated General Education course)", Credit: "3.0", Grade: "66", Status: "Complete", CompletionDate: "Winter2017"}

	//SDNE semester 2
	java2 := Course{CourseId: "PROG24178", CourseName: "Object Oriented Programming 2 – Java", Credit: "6.0", Grade: "76", Status: "Complete", CompletionDate: "Summer2018"}
	webdev2 := Course{CourseId: "SYST10199", CourseName: "Web Programming", Credit: "3.0", Grade: "55", Status: "Complete", CompletionDate: "Summer2018"}
	tele2 := Course{CourseId: "TELE33324", CourseName: "Data Network Design and Configuration – Routers & Switches", Credit: "3.0", Grade: "63", Status: "Complete", CompletionDate: "Summer2018"}
	design := Course{CourseId: "SYST15892", CourseName: "Interactive User Interface Design", Credit: "3.0", Grade: "87", Status: "Complete", CompletionDate: "Summer2018"}
	design2 := Course{CourseId: "SYST17796", CourseName: "Fundamentals of Software Design", Credit: "3.0", Grade: "85", Status: "Complete", CompletionDate: "Summer2018"}
	linux := Course{CourseId: "SYST13416", CourseName: "Linux/Unix Operating Systems", Credit: "3.0", Grade: "81", Status: "Complete", CompletionDate: "Summer2018"}

	//SDNE semester 3
	java3 := Course{CourseId: "PROG32758", CourseName: "Enterprise Java Development", Credit: "6.0", Grade: "72", Status: "Complete", CompletionDate: "Winter2018"}
	design3 := Course{CourseId: "SYST28951", CourseName: "Systems Development Methodologies", Credit: "3.0", Grade: "86", Status: "Complete", CompletionDate: "Winter2018"}
	security := Course{CourseId: "INFO24178", CourseName: "Computer and Network Security", Credit: "3.0", Grade: "57", Status: "Complete", CompletionDate: "Winter2018"}
	dba1 := Course{CourseId: "DBAS27198", CourseName: "Database Design and Implementation", Credit: "4.0", Grade: "90", Status: "Complete", CompletionDate: "Winter2018"}
	arch := Course{CourseId: "SYST26671", CourseName: "Computer Architecture", Credit: "3.0", Grade: "87", Status: "Complete", CompletionDate: "Winter2018"}
	coop := Course{CourseId: "COWT10022", CourseName: "Cooperative Education Forum", Credit: "1.0", Grade: "68", Status: "Complete", CompletionDate: "Winter2018"}
	elect1 := Course{CourseId: "GENED1", CourseName: "General Education Course (Open)", Credit: "3.0", Grade: "68", Status: "Complete", CompletionDate: "Winter2018"}

	//SDNE semester 4
	csharp := Course{CourseId: "PROG37721", CourseName: "Web Services using .NET and C# ", Credit: "6.0", Grade: "78", Status: "Complete", CompletionDate: "Fall2017"}
	webdev3 := Course{CourseId: "SYST24444", CourseName: "Mobile Web-based Applications", Credit: "3.0", Grade: "65", Status: "Complete", CompletionDate: "Fall2017"}
	pmp := Course{CourseId: "INFO20172", CourseName: "IT Project Management using PMP", Credit: "3.0", Grade: "58", Status: "Complete", CompletionDate: "Fall2017"}
	cclass := Course{CourseId: "PROG20799", CourseName: "Data Structures & Algorithm Development – C ", Credit: "6.0", Grade: "88", Status: "Complete", CompletionDate: "Fall2017"}
	dba2 := Course{CourseId: "DBAS32100", CourseName: "RDBMS Application Development (Option Elective) – Career Path", Credit: "4.0", Grade: "72", Status: "Complete", CompletionDate: "Fall2017"}
	elect2 := Course{CourseId: "GENED2", CourseName: "General Education Course (Open)", Credit: "3.0", Grade: "63", Status: "Complete", CompletionDate: "Fall2017"}

	//SDNE semester 5
	capstone1 := Course{CourseId: "INFO34049", CourseName: "Capstone Project Research Prep", Credit: "3.0", Grade: "80", Status: "Complete", CompletionDate: "Summer2017"}
	eqs := Course{CourseId: "SYST30009", CourseName: "Engineering Quality Software", Credit: "3.0", Grade: "74", Status: "Complete", CompletionDate: "Summer2017"}
	dba3 := Course{CourseId: "DBAS36206", CourseName: "Database Administration and Security", Credit: "3.0", Grade: "94", Status: "Complete", CompletionDate: "Summer2017"}
	os := Course{CourseId: "SYST20261", CourseName: "Applied Operating Systems Design", Credit: "3.0", Grade: "65", Status: "Complete", CompletionDate: "Summer2017"}
	ios1 := Course{CourseId: "PROG31632", CourseName: "Mobile iOS Application Development (Option Electives 1)", Credit: "4.0", Grade: "74", Status: "Complete", CompletionDate: "Summer2017"}
	android1 := Course{CourseId: "PROG38448", CourseName: "Mobile Java Application Development (Option Electives 1)", Credit: "3.0", Grade: "86", Status: "Complete", CompletionDate: "Summer2017"}
	innov := Course{CourseId: "INFO33551G", CourseName: "Innovation: From Idea to Execution (Mandated Elective)", Credit: "3.0", Grade: "86", Status: "Complete", CompletionDate: "Summer2017"}

	//SDNE semester 6
	capstone2 := Course{CourseId: "INFO39014", CourseName: "Capstone Project", Credit: "6.0", Grade: "76", Status: "Complete", CompletionDate: "Winter2017"}
	java4 := Course{CourseId: "PROG39599", CourseName: "Advanced Java Frameworks (Option Electives 2)", Credit: "6.0", Grade: "83", Status: "Complete", CompletionDate: "Winter2017"}
	android2 := Course{CourseId: "PROG39402", CourseName: "Advanced Mobile Application Development (Option Electives 3)", Credit: "3.0", Grade: "81", Status: "Complete", CompletionDate: "Winter2017"}
	ios2 := Course{CourseId: "PROG39856", CourseName: "Advanced Mobile iOS Development (Option Electives 3)", Credit: "6.0", Grade: "90", Status: "Complete", CompletionDate: "Winter2017"}
	math3 := Course{CourseId: "MATH32668", CourseName: "Statistics – Computer Science", Credit: "3.0", Grade: "77", Status: "Complete", CompletionDate: "Winter2017"}

	semester1 := []Course{java1, webdev1, tele1, comm1, math1, cult}
	semester2 := []Course{java2, webdev2, tele2, design, design2, linux}
	semester3 := []Course{java3, design3, security, dba1, arch, coop, elect1}
	semester4 := []Course{csharp, webdev3, pmp, cclass, dba2, elect2}
	semester5 := []Course{capstone1, eqs, dba3, os, ios1, android1, innov}
	semester6 := []Course{capstone2, java4, android2, ios2, math3}

	transcript := []Transcript{
		Transcript{ObjectType: "transcript", Id: "1", StudentID: "991348290", Institution: "SheridanCollege", Courses: courses},
		Transcript{ObjectType: "transcript", Id: "2", StudentID: "991334522", Institution: "SheridanCollege", Courses: courses2},
		Transcript{ObjectType: "transcript", Id: "3", StudentID: "991334522", Institution: "SheridanCollege", Courses: semester1},
		Transcript{ObjectType: "transcript", Id: "4", StudentID: "991334522", Institution: "HumberCollege", Courses: semester2},
		Transcript{ObjectType: "transcript", Id: "5", StudentID: "991334522", Institution: "SheridanCollege", Courses: semester3},
		Transcript{ObjectType: "transcript", Id: "6", StudentID: "991334522", Institution: "HumberCollege", Courses: semester4},
		Transcript{ObjectType: "transcript", Id: "7", StudentID: "991173683", Institution: "SheridanCollege", Courses: semester5},
		Transcript{ObjectType: "transcript", Id: "8", StudentID: "991173683", Institution: "HumberCollege", Courses: semester6},
		Transcript{ObjectType: "transcript", Id: "9", StudentID: "991173683", Institution: "HumberCollege", Courses: semester4},
		Transcript{ObjectType: "transcript", Id: "10", StudentID: "991419801", Institution: "SheridanCollege", Courses: semester5},
		Transcript{ObjectType: "transcript", Id: "11", StudentID: "991419801", Institution: "HumberCollege", Courses: semester6},
		Transcript{ObjectType: "transcript", Id: "12", StudentID: "991419801", Institution: "SheridanCollege", Courses: courses2},
	}

	i := 0
	for i < len(transcript) {
		fmt.Println("\ni is ", i)
		transcriptAsBytes, _ := json.Marshal(transcript[i])
		APIstub.PutState(transcript[i].Id, transcriptAsBytes)
		fmt.Println("Added", transcript[i])
		i = i + 1
	}

	return shim.Success(nil)
}
func (s *SmartContract) createTranscript(APIstub shim.ChaincodeStubInterface, args []string) sc.Response {

	val, ok, err := cid.GetAttributeValue(APIstub, "hf.Type")
	if err != nil {
		return shim.Error("Error retrieving attribute hf.Type, error: " + err.Error())
	}
	if !ok {
		return shim.Error("User does not have attribute hf.Type")
	}

	if val != "student" {

		transcriptArgs := len(args) - 3

		course := Course{CourseId: args[3], CourseName: args[4], Credit: args[5], Grade: args[6], Status: args[7], CompletionDate: args[8]}
		courses := []Course{course}

		if transcriptArgs > 6 {
			x := 6
			for x < transcriptArgs {
				coursePlus := Course{CourseId: args[3+x], CourseName: args[4+x], Credit: args[5+x], Grade: args[6+x], Status: args[7+x], CompletionDate: args[8+x]}
				x = x + 6

				courses = append(courses, coursePlus)

			}
		}

		transcript := Transcript{ObjectType: "transcript", Id: args[0], StudentID: args[1], Institution: args[2], Courses: courses}

		transcriptAsBytes, _ := json.Marshal(transcript)
		APIstub.PutState(args[0], transcriptAsBytes)
		return shim.Success(nil)
	}

	return shim.Error("You are not authorized to add transcript")
}

func (s *SmartContract) queryTranscriptById(APIstub shim.ChaincodeStubInterface, args []string) sc.Response {

	if len(args) < 1 {
		return shim.Error("Incorrect number of arguments. Expecting 1")
	}

	queryString := fmt.Sprintf("{\"selector\":{\"docType\":\"transcript\",\"id\":\"%s\"}}", args[0])

	queryResults, err := getQueryResultForQueryString(APIstub, queryString)
	if err != nil {
		return shim.Error(err.Error())
	}
	return shim.Success(queryResults)
}

func (s *SmartContract) queryAllTranscripts(APIstub shim.ChaincodeStubInterface) sc.Response {

	queryString := fmt.Sprintf("{\"selector\":{\"docType\":\"transcript\"}}")

	queryResults, err := getQueryResultForQueryString(APIstub, queryString)
	if err != nil {
		return shim.Error(err.Error())
	}
	return shim.Success(queryResults)

}

func (s *SmartContract) queryAllTranscriptsByInstitution(APIstub shim.ChaincodeStubInterface, args []string) sc.Response {

	queryString := fmt.Sprintf("{\"selector\":{\"docType\":\"transcript\",\"institution\":\"%s\"}}", args[0])

	queryResults, err := getQueryResultForQueryString(APIstub, queryString)
	if err != nil {
		return shim.Error(err.Error())
	}
	return shim.Success(queryResults)

}

func (s *SmartContract) queryTranscriptByStudentID(APIstub shim.ChaincodeStubInterface, args []string) sc.Response {

	queryString := fmt.Sprintf("{\"selector\":{\"docType\":\"transcript\",\"studentId\":\"%s\"}}", args[0])

	queryResults, err := getQueryResultForQueryString(APIstub, queryString)
	if err != nil {
		return shim.Error(err.Error())
	}
	return shim.Success(queryResults)

}

/*
	This function adds additional courses to the transcript specified

*/
func (s *SmartContract) addTranscriptCourse(APIstub shim.ChaincodeStubInterface, args []string) sc.Response {

	if len(args) != 7 {
		return shim.Error("Incorrect number of arguments. Expecting 6")
	}

	course := Course{CourseId: args[1], CourseName: args[2], Credit: args[3], Grade: args[4], Status: args[5], CompletionDate: args[6]}

	transcriptAsBytes, _ := APIstub.GetState(args[0])
	transcript := Transcript{}

	json.Unmarshal(transcriptAsBytes, &transcript)

	// Hold the courses that was already in the transcript
	//var courseHold = transcript.Courses

	// Appends the course entered with the existing courses in the transcript
	transcript.Courses = append(transcript.Courses, course)

	transcriptAsBytes, _ = json.Marshal(transcript)
	APIstub.PutState(args[0], transcriptAsBytes)

	return shim.Success(nil)
}

func (s *SmartContract) deleteTranscript(APIstub shim.ChaincodeStubInterface, args []string) sc.Response {

	if len(args) != 1 {
		return shim.Error("Incorrect number of arguments. Expecting 1")
	}

	A := args[0]

	transcriptAsBytes := APIstub.DelState(A)

	if transcriptAsBytes != nil {
		return shim.Error("Failed to delete state")
	}

	return shim.Success(nil)
}

func (s *SmartContract) getHistoryForTranscript(APIstub shim.ChaincodeStubInterface, args []string) sc.Response {

	if len(args) < 1 {
		return shim.Error("Incorrect number of arguments. Expecting 1")
	}

	transcriptId := args[0]

	fmt.Printf("- start getHistoryForTranscript: %s\n", transcriptId)

	// GetCreator returns marshaled serialized identity of the client
	//serializedID, _ := APIstub.GetCreator()

	//sId := &msp.SerializedIdentity{}
	//err := proto.Unmarshal(serializedID, sId)

	//if err != nil {
	//	return shim.Error(fmt.Sprintf("Could not deserialize a SerializedIdentity, err %s", err))
	//}

	resultsIterator, err := APIstub.GetHistoryForKey(transcriptId)
	if err != nil {
		return shim.Error(err.Error())
	}
	defer resultsIterator.Close()

	// buffer is a JSON array containing historic values for the marble
	var buffer bytes.Buffer
	buffer.WriteString("[")

	bArrayMemberAlreadyWritten := false
	for resultsIterator.HasNext() {
		response, err := resultsIterator.Next()
		if err != nil {
			return shim.Error(err.Error())
		}
		// Add a comma before array members, suppress it for the first array member
		if bArrayMemberAlreadyWritten == true {
			buffer.WriteString(",")
		}
		buffer.WriteString("{\"TxId\":")
		buffer.WriteString("\"")
		buffer.WriteString(response.TxId)
		buffer.WriteString("\"")

		buffer.WriteString(", \"Value\":")
		// if it was a delete operation on given key, then we need to set the
		//corresponding value null. Else, we will write the response.Value
		//as-is (as the Value itself a JSON marble)
		if response.IsDelete {
			buffer.WriteString("null")
		} else {
			buffer.WriteString(string(response.Value))
		}

		buffer.WriteString(", \"Timestamp\":")
		buffer.WriteString("\"")
		buffer.WriteString(time.Unix(response.Timestamp.Seconds, int64(response.Timestamp.Nanos)).String())
		buffer.WriteString("\"")

		buffer.WriteString(", \"IsDelete\":")
		buffer.WriteString("\"")
		buffer.WriteString(strconv.FormatBool(response.IsDelete))
		buffer.WriteString("\"")
		/*
			buffer.WriteString(", \"Creator\":")
			buffer.WriteString("\"")
			buffer.WriteString(string(serializedID))
			buffer.WriteString("\"")
		*/
		buffer.WriteString("}")
		bArrayMemberAlreadyWritten = true
	}
	buffer.WriteString("]")

	fmt.Printf("- getHistoryForTranscript returning:\n%s\n", buffer.String())

	return shim.Success(buffer.Bytes())
}

func (s *SmartContract) getAttribute(APIstub shim.ChaincodeStubInterface) sc.Response {
	var attributeName string

	val, ok, err := cid.GetAttributeValue(APIstub, "uid")
	if err != nil {
		return shim.Error("Error retrieving attribute " + attributeName + ", error: " + err.Error())
	}
	if !ok {
		return shim.Error("User does not have attribute " + attributeName)
	}

	val2, ok2, err2 := cid.GetAttributeValue(APIstub, "hf.Type")
	if err2 != nil {
		return shim.Error("Error retrieving attribute " + attributeName + ", error: " + err.Error())
	}
	if !ok2 {
		return shim.Error("User does not have attribute " + attributeName)
	}

	respJSON := map[string]interface{}{}
	respJSON["uid"] = string([]byte(val))
	respJSON["role"] = string([]byte(val2))
	b, _ := json.Marshal(respJSON)
	return shim.Success(b)

}

//============================================= COURSES =============================================
func (s *SmartContract) queryCourse(APIstub shim.ChaincodeStubInterface, args []string) sc.Response {
	//var courseId string // Entities

	//var err error

	if len(args) != 1 {
		return shim.Error("Incorrect number of arguments. Expecting 1")
	}

	courseAsBytes, _ := APIstub.GetState(args[0])

	return shim.Success(courseAsBytes)
}

func (s *SmartContract) queryCourseById(APIstub shim.ChaincodeStubInterface, args []string) sc.Response {

	if len(args) < 1 {
		return shim.Error("Incorrect number of arguments. Expecting 1")
	}

	queryString := fmt.Sprintf("{\"selector\":{\"docType\":\"course\",\"id\":\"%s\"}}", args[0])

	queryResults, err := getQueryResultForQueryString(APIstub, queryString)
	if err != nil {
		return shim.Error(err.Error())
	}
	return shim.Success(queryResults)
}

func (s *SmartContract) queryAllCourses(APIstub shim.ChaincodeStubInterface) sc.Response {

	queryString := fmt.Sprintf("{\"selector\":{\"docType\":\"course\"}}")

	queryResults, err := getQueryResultForQueryString(APIstub, queryString)
	if err != nil {
		return shim.Error(err.Error())
	}
	return shim.Success(queryResults)

}

func (s *SmartContract) createCourse(APIstub shim.ChaincodeStubInterface, args []string) sc.Response {

	if len(args) != 5 {
		return shim.Error("Incorrect number of arguments. Expecting 4")
	}

	// /course := Course{ObjectType: "course", Id: args[0], Name: args[1], Description: args[2], Credits: args[3], Institution: args[4]}
	//course1 := Course{ObjectType: "course", Id: args[0], Name: args[1], Description: args[2], Credits: args[3], Institution: args[4]}
	/*
		courses := []Course{course, course1}
		transcript := Transcript{Id: "34", Student: "sd", Institution: "sadsa", Courses: courses}

		courseAsBytes, _ := json.Marshal(transcript)
		APIstub.PutState(args[0], courseAsBytes)*/

	return shim.Success(nil)
}

func (s *SmartContract) deleteCourse(APIstub shim.ChaincodeStubInterface, args []string) sc.Response {

	if len(args) != 1 {
		return shim.Error("Incorrect number of arguments. Expecting 1")
	}

	A := args[0]

	courseAsBytes := APIstub.DelState(A)

	if courseAsBytes != nil {
		return shim.Error("Failed to delete state")
	}

	return shim.Success(nil)
}

//============================================= STUDENTS =============================================
func (s *SmartContract) queryStudent(APIstub shim.ChaincodeStubInterface, args []string) sc.Response {

	if len(args) != 1 {
		return shim.Error("Incorrect number of arguments. Expecting 1")
	}

	studentAsBytes, _ := APIstub.GetState(args[0])
	return shim.Success(studentAsBytes)
}

func (s *SmartContract) createStudent(APIstub shim.ChaincodeStubInterface, args []string) sc.Response {

	if len(args) != 6 {
		return shim.Error("Incorrect number of arguments. Expecting 6")
	}

	var student = Student{Id: args[1], FirstName: args[2], LastName: args[3], MiddleName: args[4]}

	studentAsBytes, _ := json.Marshal(student)
	APIstub.PutState(args[0], studentAsBytes)

	return shim.Success(nil)
}

func (s *SmartContract) queryAllStudents(APIstub shim.ChaincodeStubInterface) sc.Response {

	startKey := "STUDENT0"
	endKey := "STUDENT999"

	resultsIterator, err := APIstub.GetStateByRange(startKey, endKey)
	if err != nil {
		return shim.Error(err.Error())
	}
	defer resultsIterator.Close()

	// buffer is a JSON array containing QueryResults
	var buffer bytes.Buffer
	buffer.WriteString("[")

	bArrayMemberAlreadyWritten := false
	for resultsIterator.HasNext() {
		queryResponse, err := resultsIterator.Next()
		if err != nil {
			return shim.Error(err.Error())
		}
		// Add a comma before array members, suppress it for the first array member
		if bArrayMemberAlreadyWritten == true {
			buffer.WriteString(",")
		}
		buffer.WriteString("{\"Key\":")
		buffer.WriteString("\"")
		buffer.WriteString(queryResponse.Key)
		buffer.WriteString("\"")

		buffer.WriteString(", \"Record\":")
		// Record is a JSON object, so we write as-is
		buffer.WriteString(string(queryResponse.Value))
		buffer.WriteString("}")
		bArrayMemberAlreadyWritten = true
	}
	buffer.WriteString("]")

	fmt.Printf("- queryAllStudents:\n%s\n", buffer.String())

	return shim.Success(buffer.Bytes())
}

func (s *SmartContract) deleteStudent(APIstub shim.ChaincodeStubInterface, args []string) sc.Response {

	if len(args) != 1 {
		return shim.Error("Incorrect number of arguments. Expecting 1")
	}

	A := args[0]

	studentAsBytes := APIstub.DelState(A)

	if studentAsBytes != nil {
		return shim.Error("Failed to delete state")
	}

	return shim.Success(nil)
}

// =========================================================================================
// getQueryResultForQueryString executes the passed in query string.
// Result set is built and returned as a byte array containing the JSON results.
// =========================================================================================
func getQueryResultForQueryString(stub shim.ChaincodeStubInterface, queryString string) ([]byte, error) {

	fmt.Printf("- getQueryResultForQueryString queryString:\n%s\n", queryString)

	resultsIterator, err := stub.GetQueryResult(queryString)
	if err != nil {
		return nil, err
	}
	defer resultsIterator.Close()

	// buffer is a JSON array containing QueryRecords
	var buffer bytes.Buffer
	buffer.WriteString("[")

	bArrayMemberAlreadyWritten := false
	for resultsIterator.HasNext() {
		queryResponse, err := resultsIterator.Next()
		if err != nil {
			return nil, err
		}
		// Add a comma before array members, suppress it for the first array member
		if bArrayMemberAlreadyWritten == true {
			buffer.WriteString(",")
		}
		buffer.WriteString("{\"Key\":")
		buffer.WriteString("\"")
		buffer.WriteString(queryResponse.Key)
		buffer.WriteString("\"")

		buffer.WriteString(", \"Record\":")
		// Record is a JSON object, so we write as-is
		buffer.WriteString(string(queryResponse.Value))
		buffer.WriteString("}")
		bArrayMemberAlreadyWritten = true
	}
	buffer.WriteString("]")

	fmt.Printf("- getQueryResultForQueryString queryResult:\n%s\n", buffer.String())

	return buffer.Bytes(), nil
}

// The main function is only relevant in unit test mode. Only included here for completeness.
func main() {

	// Create a new Smart Contract
	err := shim.Start(new(SmartContract))
	if err != nil {
		fmt.Printf("Error creating new Smart Contract: %s", err)
	}
}
