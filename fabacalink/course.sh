#!/bin/bash

# Print the usage message
function printHelp() {
  echo "Options: "
  echo "    To create a course:"
  echo "        course.sh create"
  echo "    To query a course:"
  echo "        course.sh query <CourseID>"              
  echo "    To query all courses:"
  echo "        course.sh queryAll"
  echo "    To delete a specific course"
  echo "        course.sh delete <CourseID>"
}

MODE=$1

if  [ "$MODE" = "create" ]
then
    echo "Creating Course"
    node functions/course/invokeCourse.js
elif [ "$MODE" = "query" ]
then
    echo "Query Course"
    node functions/course/queryCourse.js $2
elif [ "$MODE" = "queryAll" ]
then
    echo "Querying All Courses"
    node functions/course/queryAllCourses.js
elif [ "$MODE" = "delete" ]
then
    echo "Delete Course"
    node functions/course/deleteCourse.js $2
else
  printHelp
  exit 1
fi
