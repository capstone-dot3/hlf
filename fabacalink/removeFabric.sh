#!/bin/bash

#echo =============== Killing stale or active containers ===============
docker rm -f $(docker ps -aq)

echo  
echo =============== Clearing any cached networks ===============
# Press 'y' when prompted by the command
docker network prune

echo  
echo ==== Deleting the underlying chaincode image for the acalink smart contract ====
docker rmi dev-peer0.sheridanc.acalink.com-acalink-1.0-fadee20e83cfce49d5a0e2c3e13f33dfb9a67a446a024bc24fe8c822ee17c647     