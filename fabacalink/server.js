// ExpressJS Setup
//$ npm install express --save
const cors = require('cors');
const express = require('express');
const crypto = require('crypto');
const app = express();

// app.use(allowCrossDomain);
// app.options('*', cors());
var bodyParser = require('body-parser');
app.use(bodyParser.json()); // support json encoded bodies
app.use(bodyParser.urlencoded({ extended: true })); // support encoded bodies
// Constants
const PORT = 9091;
// const HOST = 'localhost';
const HOST = '159.203.30.102';


app.use(cors());
app.use(function(req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header('Access-Control-Allow-Methods', 'DELETE, PUT');
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    if ('OPTIONS' == req.method) {
       res.sendStatus(200);
     }
     else {
       next();
     }});


 // Hyperledger Bridge
var Fabric_Client = require('fabric-client');
var Fabric_CA_Client = require('fabric-ca-client');
var path = require('path');
var util = require('util');
var os = require('os');

//
var fabric_client = new Fabric_Client();

var fabric_ca_client = null;
var admin_user = null;
var member_user = null;

// setup the fabric network
var channel = fabric_client.newChannel('institutionch');
var peer = fabric_client.newPeer('grpc://'+HOST+':7051');
channel.addPeer(peer);
var order = fabric_client.newOrderer('grpc://'+HOST+':7050')
channel.addOrderer(order);


//
var member_user = null;
var store_path = path.join(__dirname, 'hfc-key-store');
console.log('Store path:'+store_path);
var tx_id = null;


//Attach the middleware

// app.all('/', function(req, res, next) {
//   res.header("Access-Control-Allow-Origin", "*");
//   res.header("Access-Control-Allow-Headers", "X-Requested-With");
//   next();
//  });


app.get('/api/queries/queryall', function (req, res) {
		// create the key value store as defined in the fabric-client/config/default.json 'key-value-store' setting
		Fabric_Client.newDefaultKeyValueStore({ path: store_path
		}).then((state_store) => {
			// assign the store to the fabric client
			fabric_client.setStateStore(state_store);
			var crypto_suite = Fabric_Client.newCryptoSuite();
			// use the same location for the state store (where the users' certificate are kept)
			// and the crypto store (where the users' keys are kept)
			var crypto_store = Fabric_Client.newCryptoKeyStore({path: store_path});
			crypto_suite.setCryptoKeyStore(crypto_store);
			fabric_client.setCryptoSuite(crypto_suite);
            var username = req.query.username;
			// get the enrolled user from persistence, this user will sign all requests
			return fabric_client.getUserContext(username, true);
		}).then((user_from_store) => {
			if (user_from_store && user_from_store.isEnrolled()) {
				console.log('Successfully loaded sheridanc from persistence');
				member_user = user_from_store;
			} else {
				throw new Error('Failed to get sheridanc.... run registerUser.js');
			}

			// queryCar chaincode function - requires 1 argument, ex: args: ['CAR4'],
			// queryAllCars chaincode function - requires no arguments , ex: args: [''],
			const request = {
				//targets : --- letting this default to the peers assigned to the channel
				chaincodeId: 'acalink',
				fcn: 'queryAllTranscripts',
                args: []
                //http://localhost:9091/api/queries/queryall
			};
			// send the query proposal to the peer
			return channel.queryByChaincode(request);
		}).then((query_responses) => {
			console.log("Query has completed, checking results");
			console.log(query_responses);
		        // query_responses could have more than one  results if there multiple peers were used as targets
			if (query_responses && query_responses.length == 1) {
				if (query_responses[0] instanceof Error) {
					console.error("error from query = ", JSON.parse(query_responses[0].toString()));
				} else {
					console.log("Response is ", JSON.parse(query_responses[0].toString()));
				}
			} else {
				console.log("No payloads were returned from query");
			}
			res.status(200).json({response: JSON.parse(query_responses[0].toString())});
		}).catch(function(err) {
		    res.status(500).json({error: err.toString()})
		  })
});

app.get('/api/queries/queryallinstitution/q', function (req, res) {
    // create the key value store as defined in the fabric-client/config/default.json 'key-value-store' setting
    Fabric_Client.newDefaultKeyValueStore({ path: store_path
    }).then((state_store) => {
        // assign the store to the fabric client
        fabric_client.setStateStore(state_store);
        var crypto_suite = Fabric_Client.newCryptoSuite();
        // use the same location for the state store (where the users' certificate are kept)
        // and the crypto store (where the users' keys are kept)
        var crypto_store = Fabric_Client.newCryptoKeyStore({path: store_path});
        crypto_suite.setCryptoKeyStore(crypto_store);
        fabric_client.setCryptoSuite(crypto_suite);

        // get the enrolled user from persistence, this user will sign all requests
        return fabric_client.getUserContext(req.query.username, true);
    }).then((user_from_store) => {
        if (user_from_store && user_from_store.isEnrolled()) {
            console.log('Successfully loaded sheridanc from persistence');
            member_user = user_from_store;
        } else {
            throw new Error('Failed to get sheridanc.... run registerUser.js');
        }

        // queryCar chaincode function - requires 1 argument, ex: args: ['CAR4'],
        // queryAllCars chaincode function - requires no arguments , ex: args: [''],
        const request = {
            //targets : --- letting this default to the peers assigned to the channel
            chaincodeId: 'acalink',
            fcn: 'queryAllTranscriptsByInstitution',
            args: [req.query.institution]
            //http://localhost:9091/api/queries/queryallinstitution/p?institution=2
        };
        // send the query proposal to the peer
        return channel.queryByChaincode(request);
    }).then((query_responses) => {
        console.log("Query has completed, checking results");
        console.log(query_responses);
            // query_responses could have more than one  results if there multiple peers were used as targets
        if (query_responses && query_responses.length == 1) {
            if (query_responses[0] instanceof Error) {
                console.error("error from query = ", JSON.parse(query_responses[0].toString()));
            } else {
                console.log("Response is ", JSON.parse(query_responses[0].toString()));
            }
        } else {
            console.log("No payloads were returned from query");
        }
        res.status(200).json({response: JSON.parse(query_responses[0].toString())});
    }).catch(function(err) {
        res.status(500).json({error: err.toString()})
      })
});

app.get('/api/queries/querybystudentid/p', function (req, res) {
    // create the key value store as defined in the fabric-client/config/default.json 'key-value-store' setting
    Fabric_Client.newDefaultKeyValueStore({ path: store_path
    }).then((state_store) => {
        // assign the store to the fabric client
        fabric_client.setStateStore(state_store);
        var crypto_suite = Fabric_Client.newCryptoSuite();
        // use the same location for the state store (where the users' certificate are kept)
        // and the crypto store (where the users' keys are kept)
        var crypto_store = Fabric_Client.newCryptoKeyStore({path: store_path});
        crypto_suite.setCryptoKeyStore(crypto_store);
        fabric_client.setCryptoSuite(crypto_suite);

        // get the enrolled user from persistence, this user will sign all requests
        return fabric_client.getUserContext(req.query.username, true);
    }).then((user_from_store) => {
        if (user_from_store && user_from_store.isEnrolled()) {
            console.log('Successfully loaded sheridanc from persistence');
            member_user = user_from_store;
        } else {
            throw new Error('Failed to get sheridanc.... run registerUser.js');
        }

        // queryCar chaincode function - requires 1 argument, ex: args: ['CAR4'],
        // queryAllCars chaincode function - requires no arguments , ex: args: [''],
        const request = {
            //targets : --- letting this default to the peers assigned to the channel
            chaincodeId: 'acalink',
            fcn: 'queryTranscriptByStudentID',
            args: [req.query.studentID]
            //http://localhost:9091/api/queries/queryallinstitution/p?institution=2
        };
        // send the query proposal to the peer
        return channel.queryByChaincode(request);
    }).then((query_responses) => {
        console.log("Query has completed, checking results");
        console.log(query_responses);
            // query_responses could have more than one  results if there multiple peers were used as targets
        if (query_responses && query_responses.length == 1) {
            if (query_responses[0] instanceof Error) {
                console.error("error from query = ", JSON.parse(query_responses[0].toString()));
            } else {
                console.log("Response is ", JSON.parse(query_responses[0].toString()));
            }
        } else {
            console.log("No payloads were returned from query");
        }
        res.status(200).json({response: JSON.parse(query_responses[0].toString())});
    }).catch(function(err) {
        res.status(500).json({error: err.toString()})
      })
});



app.get('/api/queries/query/p', function (req, res) {
    // create the key value store as defined in the fabric-client/config/default.json 'key-value-store' setting
    Fabric_Client.newDefaultKeyValueStore({ path: store_path
    }).then((state_store) => {
        // assign the store to the fabric client
        fabric_client.setStateStore(state_store);
        var crypto_suite = Fabric_Client.newCryptoSuite();
        // use the same location for the state store (where the users' certificate are kept)
        // and the crypto store (where the users' keys are kept)
        var crypto_store = Fabric_Client.newCryptoKeyStore({path: store_path});
        crypto_suite.setCryptoKeyStore(crypto_store);
        fabric_client.setCryptoSuite(crypto_suite);

        // get the enrolled user from persistence, this user will sign all requests
        return fabric_client.getUserContext(req.query.username, true);
    }).then((user_from_store) => {
        if (user_from_store && user_from_store.isEnrolled()) {
            console.log('Successfully loaded sheridanc from persistence');
            member_user = user_from_store;
        } else {
            throw new Error('Failed to get sheridanc.... run registerUser.js');
        }

        // queryCar chaincode function - requires 1 argument, ex: args: ['CAR4'],
        // queryAllCars chaincode function - requires no arguments , ex: args: [''],
        const request = {
            //targets : --- letting this default to the peers assigned to the channel
            chaincodeId: 'acalink',
		    fcn: 'queryTranscriptById',
            args: [req.query.id]
            //http://localhost:9091/api/queries/query/p?id=7
        };
        // send the query proposal to the peer
        return channel.queryByChaincode(request);
    }).then((query_responses) => {
        console.log("Query has completed, checking results");
        console.log(query_responses);
            // query_responses could have more than one  results if there multiple peers were used as targets
        if (query_responses && query_responses.length == 1) {
            if (query_responses[0] instanceof Error) {
                console.error("error from query = ", JSON.parse(query_responses[0].toString()));
            } else {
                console.log("Response is ", JSON.parse(query_responses[0].toString()));
            }
        } else {
            console.log("No payloads were returned from query");
        }
        res.status(200).json({response: JSON.parse(query_responses[0].toString())});
    }).catch(function(err) {
        res.status(500).json({error: err.toString()})
      })
});

//Attach the middleware
//app.use( bodyParser.json() );
app.get('/api/queries/history/p', function (req, res) {
    // create the key value store as defined in the fabric-client/config/default.json 'key-value-store' setting
    Fabric_Client.newDefaultKeyValueStore({ path: store_path
    }).then((state_store) => {
        // assign the store to the fabric client
        fabric_client.setStateStore(state_store);
        var crypto_suite = Fabric_Client.newCryptoSuite();
        // use the same location for the state store (where the users' certificate are kept)
        // and the crypto store (where the users' keys are kept)
        var crypto_store = Fabric_Client.newCryptoKeyStore({path: store_path});
        crypto_suite.setCryptoKeyStore(crypto_store);
        fabric_client.setCryptoSuite(crypto_suite);

        // get the enrolled user from persistence, this user will sign all requests
        return fabric_client.getUserContext(req.query.username, true);
    }).then((user_from_store) => {
        if (user_from_store && user_from_store.isEnrolled()) {
            console.log('Successfully loaded sheridanc from persistence');
            member_user = user_from_store;
        } else {
            throw new Error('Failed to get sheridanc.... run registerUser.js');
        }

        // queryCar chaincode function - requires 1 argument, ex: args: ['CAR4'],
        // queryAllCars chaincode function - requires no arguments , ex: args: [''],
        const request = {
            //targets : --- letting this default to the peers assigned to the channel
            chaincodeId: 'acalink',
		    fcn: 'getHistoryForTranscript',
            args: [req.query.id]
            //http://localhost:9091/api/queries/history/p?id=7
        };
        // send the query proposal to the peer
        return channel.queryByChaincode(request);
    }).then((query_responses) => {
        console.log("Query has completed, checking results");
        console.log(query_responses);
            // query_responses could have more than one  results if there multiple peers were used as targets
        if (query_responses && query_responses.length == 1) {
            if (query_responses[0] instanceof Error) {
                console.error("error from query = ", JSON.parse(query_responses[0].toString()));
            } else {
                console.log("Response is ", JSON.parse(query_responses[0].toString()));
            }
        } else {
            console.log("No payloads were returned from query");
        }
        res.status(200).json({response: JSON.parse(query_responses[0].toString())});
    }).catch(function(err) {
        res.status(500).json({error: err.toString()})
      })
});

//Attach the middleware
app.use( bodyParser.json() );
app.post('/api/invoke/addtranscript', function (req, res) {
    var username = req.body.username;
    var id = crypto.randomBytes(16).toString("hex");
    var institution = req.body.institution;
    var studentId = req.body.studentId;

    var args = [id, studentId, institution];

    var i;
    
    for (i = 0; i < Object.keys(req.body.courses).length; i++) {
            var courseId = req.body.courses[i].courseId;
            var courseName = req.body.courses[i].courseName;
            var credit = req.body.courses[i].credit;
            var grade = req.body.courses[i].grade;
            var status = req.body.courses[i].status;
            var completionDate = req.body.courses[i].completionDate;
            args.push(courseId);
            args.push(courseName);
            args.push(credit);
            args.push(grade);
            args.push(status);
            args.push(completionDate)
    }
    /*
    var id = req.body.id;
    var studentId = req.body.student;
    var institution = req.body.institution;

    var args = [id, studentId, institution];


    var i;
    for (i = 0; i < Object.keys(req.body.courses).length; i++) { 
        args.push(req.body.courses[i]);
    }
    */

    // create the key value store as defined in the fabric-client/config/default.json 'key-value-store' setting
    Fabric_Client.newDefaultKeyValueStore({ path: store_path
    }).then((state_store) => {
        // assign the store to the fabric client
        fabric_client.setStateStore(state_store);
        var crypto_suite = Fabric_Client.newCryptoSuite();
        // use the same location for the state store (where the users' certificate are kept)
        // and the crypto store (where the users' keys are kept)
        var crypto_store = Fabric_Client.newCryptoKeyStore({path: store_path});
        crypto_suite.setCryptoKeyStore(crypto_store);
        fabric_client.setCryptoSuite(crypto_suite);

        // get the enrolled user from persistence, this user will sign all requests
        return fabric_client.getUserContext(username, true);
    }).then((user_from_store) => {
        if (user_from_store && user_from_store.isEnrolled()) {
            console.log('Successfully loaded sheridanc from persistence');
            member_user = user_from_store;
        } else {
            throw new Error('Failed to get sheridanc.... run registerUser.js');
        }


        // get a transaction id object based on the current user assigned to fabric client
        tx_id = fabric_client.newTransactionID();
        console.log("Assigning transaction_id: ", tx_id._transaction_id);


        // queryCar chaincode function - requires 1 argument, ex: args: ['CAR4'],
        // queryAllCars chaincode function - requires no arguments , ex: args: [''],
        const request = {
            //targets : --- letting this default to the peers assigned to the channel
            chaincodeId: 'acalink',
		    fcn: 'createTranscript',
            args,//: [id, studentId, institution, courseId, courseName, credit, grade, institutionTaken, completionDate],
            chainId: 'institutionch',
		    txId: tx_id
            //http://localhost:9091/api/invoke/addtranscript
        };
        
        // send the transaction proposal to the peers
	    return channel.sendTransactionProposal(request);
    }).then((results) => {
        var proposalResponses = results[0];
        var proposal = results[1];
        let isProposalGood = false;

        if (proposalResponses && proposalResponses[0].response &&
            proposalResponses[0].response.status === 200) {
                isProposalGood = true;
                console.log('Transaction proposal was good');
            } else {
                console.error('Transaction proposal was bad');
            }
        if (isProposalGood) {
            console.log(util.format(
                'Successfully sent Proposal and received ProposalResponse: Status - %s, message - "%s"',
                proposalResponses[0].response.status, proposalResponses[0].response.message));

            // build up the request for the orderer to have the transaction committed
            var request = {
                proposalResponses: proposalResponses,
                proposal: proposal
            };

            // set the transaction listener and set a timeout of 30 sec
            // if the transaction did not get committed within the timeout period,
            // report a TIMEOUT status
            var transaction_id_string = tx_id.getTransactionID(); //Get the transaction ID string to be used by the event processing
            var promises = [];

            var sendPromise = channel.sendTransaction(request);
            promises.push(sendPromise); //we want the send transaction first, so that we know where to check status

            // get an eventhub once the fabric client has a user assigned. The user
            // is required bacause the event registration must be signed
            let event_hub = fabric_client.newEventHub();
            event_hub.setPeerAddr('grpc://'+HOST+':7053');

            // using resolve the promise so that result status may be processed
            // under the then clause rather than having the catch clause process
            // the status
            let txPromise = new Promise((resolve, reject) => {
                let handle = setTimeout(() => {
                    event_hub.disconnect();
                    resolve({event_status : 'TIMEOUT'}); //we could use reject(new Error('Trnasaction did not complete within 30 seconds'));
                }, 3000);
                event_hub.connect();
                event_hub.registerTxEvent(transaction_id_string, (tx, code) => {
                    // this is the callback for transaction event status
                    // first some clean up of event listener
                    clearTimeout(handle);
                    event_hub.unregisterTxEvent(transaction_id_string);
                    event_hub.disconnect();

                    // now let the application know what happened
                    var return_status = {event_status : code, tx_id : transaction_id_string};
                    if (code !== 'VALID') {
                        console.error('The transaction was invalid, code = ' + code);
                        resolve(return_status); // we could use reject(new Error('Problem with the tranaction, event status ::'+code));
                    } else {
                        console.log('The transaction has been committed on peer ' + event_hub._ep._endpoint.addr);
                        resolve(return_status);
                    }
                }, (err) => {
                    //this is the callback if something goes wrong with the event registration or processing
                    reject(new Error('There was a problem with the eventhub ::'+err));
                });
            });
            promises.push(txPromise);

            return Promise.all(promises);
        } else {
            console.error('Failed to send Proposal or receive valid response. Response null or status is not 200. exiting...');
            throw new Error('Failed to send Proposal or receive valid response. Response null or status is not 200. exiting...');
        }
    }).then((results) => {
        console.log('Send transaction promise and event listener promise have completed');
        // check the results in the order the promises were added to the promise all list
        if (results && results[0] && results[0].status === 'SUCCESS') {
            console.log('Successfully sent transaction to the orderer.');
        } else {
            console.error('Failed to order the transaction. Error code: ' + results[0].status);
        }
    
        if(results && results[1] && results[1].event_status === 'VALID') {
            console.log('Successfully committed the change to the ledger by the peer');
        } else {
            console.log('Transaction failed to be committed to the ledger due to ::'+results[1].event_status);
        }


        var json = '{"id":"' + id + '", "studentId":"' + req.body.studentId + '"}';
        
        res.status(200).json({response: JSON.parse(json)});
        //res.status(200).send("Successfully created transcript for: id: " +  id + " student: " + req.body.studentId);

    }).catch(function(err) {
        var json = '{"id":"' + id + '", "studentId":"' + req.body.studentId + '"}';
        
        res.status(500).json({response: JSON.parse(json)});
        //res.status(500).json({error: err.toString()})
      })
});


app.post('/api/invoke/addcourse', function (req, res) {
    //var paramSize = Object.keys(req.body).length;
    var id = req.body.id;
    /*var courseId = req.body.courseid;
    var courseName = req.body.coursename;
    var credit = req.body.credit;
    var grade = req.body.grade;
    var institutionTaken = req.body.institutiontaken;
    var completionDate = req.body.completiondate;*/
    var courseId = req.body.courses[0].courseId;
    var courseName = req.body.courses[0].courseName;
    var credit = req.body.courses[0].credit;
    var grade = req.body.courses[0].grade;
    var status = req.body.courses[0].institutionTaken;
    var completionDate = req.body.courses[0].completionDate;
    

    // create the key value store as defined in the fabric-client/config/default.json 'key-value-store' setting
    Fabric_Client.newDefaultKeyValueStore({ path: store_path
    }).then((state_store) => {
        // assign the store to the fabric client
        fabric_client.setStateStore(state_store);
        var crypto_suite = Fabric_Client.newCryptoSuite();
        // use the same location for the state store (where the users' certificate are kept)
        // and the crypto store (where the users' keys are kept)
        var crypto_store = Fabric_Client.newCryptoKeyStore({path: store_path});
        crypto_suite.setCryptoKeyStore(crypto_store);
        fabric_client.setCryptoSuite(crypto_suite);

        // get the enrolled user from persistence, this user will sign all requests
        return fabric_client.getUserContext(req.query.username, true);
    }).then((user_from_store) => {
        if (user_from_store && user_from_store.isEnrolled()) {
            console.log('Successfully loaded sheridanc from persistence');
            member_user = user_from_store;
        } else {
            throw new Error('Failed to get sheridanc.... run registerUser.js');
        }


        // get a transaction id object based on the current user assigned to fabric client
        tx_id = fabric_client.newTransactionID();
        console.log("Assigning transaction_id: ", tx_id._transaction_id);


        // queryCar chaincode function - requires 1 argument, ex: args: ['CAR4'],
        // queryAllCars chaincode function - requires no arguments , ex: args: [''],
        const request = {
            //targets : --- letting this default to the peers assigned to the channel
            chaincodeId: 'acalink',
		    fcn: 'addTranscriptCourse',
            args: [id, courseId, courseName, credit, grade, status, completionDate],
            chainId: 'institutionch',
		    txId: tx_id
            //http://localhost:9091/api/invoke/addcourse
        };
        
        // send the transaction proposal to the peers
	    return channel.sendTransactionProposal(request);
    }).then((results) => {
        var proposalResponses = results[0];
        var proposal = results[1];
        let isProposalGood = false;

        if (proposalResponses && proposalResponses[0].response &&
            proposalResponses[0].response.status === 200) {
                isProposalGood = true;
                console.log('Transaction proposal was good');
            } else {
                console.error('Transaction proposal was bad');
            }
        if (isProposalGood) {
            console.log(util.format(
                'Successfully sent Proposal and received ProposalResponse: Status - %s, message - "%s"',
                proposalResponses[0].response.status, proposalResponses[0].response.message));

            // build up the request for the orderer to have the transaction committed
            var request = {
                proposalResponses: proposalResponses,
                proposal: proposal
            };

            // set the transaction listener and set a timeout of 30 sec
            // if the transaction did not get committed within the timeout period,
            // report a TIMEOUT status
            var transaction_id_string = tx_id.getTransactionID(); //Get the transaction ID string to be used by the event processing
            var promises = [];

            var sendPromise = channel.sendTransaction(request);
            promises.push(sendPromise); //we want the send transaction first, so that we know where to check status

            // get an eventhub once the fabric client has a user assigned. The user
            // is required bacause the event registration must be signed
            let event_hub = fabric_client.newEventHub();
            event_hub.setPeerAddr('grpc://localhost:7053');

            // using resolve the promise so that result status may be processed
            // under the then clause rather than having the catch clause process
            // the status
            let txPromise = new Promise((resolve, reject) => {
                let handle = setTimeout(() => {
                    event_hub.disconnect();
                    resolve({event_status : 'TIMEOUT'}); //we could use reject(new Error('Trnasaction did not complete within 30 seconds'));
                }, 3000);
                event_hub.connect();
                event_hub.registerTxEvent(transaction_id_string, (tx, code) => {
                    // this is the callback for transaction event status
                    // first some clean up of event listener
                    clearTimeout(handle);
                    event_hub.unregisterTxEvent(transaction_id_string);
                    event_hub.disconnect();

                    // now let the application know what happened
                    var return_status = {event_status : code, tx_id : transaction_id_string};
                    if (code !== 'VALID') {
                        console.error('The transaction was invalid, code = ' + code);
                        resolve(return_status); // we could use reject(new Error('Problem with the tranaction, event status ::'+code));
                    } else {
                        console.log('The transaction has been committed on peer ' + event_hub._ep._endpoint.addr);
                        resolve(return_status);
                    }
                }, (err) => {
                    //this is the callback if something goes wrong with the event registration or processing
                    reject(new Error('There was a problem with the eventhub ::'+err));
                });
            });
            promises.push(txPromise);

            return Promise.all(promises);
        } else {
            console.error('Failed to send Proposal or receive valid response. Response null or status is not 200. exiting...');
            throw new Error('Failed to send Proposal or receive valid response. Response null or status is not 200. exiting...');
        }
    }).then((results) => {
        console.log('Send transaction promise and event listener promise have completed');
        // check the results in the order the promises were added to the promise all list
        if (results && results[0] && results[0].status === 'SUCCESS') {
            console.log('Successfully sent transaction to the orderer.');
        } else {
            console.error('Failed to order the transaction. Error code: ' + results[0].status);
        }
    
        if(results && results[1] && results[1].event_status === 'VALID') {
            console.log('Successfully committed the change to the ledger by the peer');
        } else {
            console.log('Transaction failed to be committed to the ledger due to ::'+results[1].event_status);
        }
        res.status(200).send("Successfully created transcript for: id: " + req.body.id);

    }).catch(function(err) {
        res.status(500).json({error: err.toString()})
      })
});

app.post('/api/invoke/delete', function (req, res) {
    //var paramSize = Object.keys(req.body).length;
    var id = req.body.id;

    // create the key value store as defined in the fabric-client/config/default.json 'key-value-store' setting
    Fabric_Client.newDefaultKeyValueStore({ path: store_path
    }).then((state_store) => {
        // assign the store to the fabric client
        fabric_client.setStateStore(state_store);
        var crypto_suite = Fabric_Client.newCryptoSuite();
        // use the same location for the state store (where the users' certificate are kept)
        // and the crypto store (where the users' keys are kept)
        var crypto_store = Fabric_Client.newCryptoKeyStore({path: store_path});
        crypto_suite.setCryptoKeyStore(crypto_store);
        fabric_client.setCryptoSuite(crypto_suite);

        // get the enrolled user from persistence, this user will sign all requests
        return fabric_client.getUserContext(req.query.username, true);
    }).then((user_from_store) => {
        if (user_from_store && user_from_store.isEnrolled()) {
            console.log('Successfully loaded sheridanc from persistence');
            member_user = user_from_store;
        } else {
            throw new Error('Failed to get sheridanc.... run registerUser.js');
        }


        // get a transaction id object based on the current user assigned to fabric client
        tx_id = fabric_client.newTransactionID();
        console.log("Assigning transaction_id: ", tx_id._transaction_id);


        // queryCar chaincode function - requires 1 argument, ex: args: ['CAR4'],
        // queryAllCars chaincode function - requires no arguments , ex: args: [''],
        const request = {
            //targets : --- letting this default to the peers assigned to the channel
            chaincodeId: 'acalink',
		    fcn: 'deleteTranscript',
            args: [id],
            chainId: 'institutionch',
		    txId: tx_id
            //http://localhost:9091/api/invoke/delete
        };
        
        // send the transaction proposal to the peers
	    return channel.sendTransactionProposal(request);
    }).then((results) => {
        var proposalResponses = results[0];
        var proposal = results[1];
        let isProposalGood = false;

        if (proposalResponses && proposalResponses[0].response &&
            proposalResponses[0].response.status === 200) {
                isProposalGood = true;
                console.log('Transaction proposal was good');
            } else {
                console.error('Transaction proposal was bad');
            }
        if (isProposalGood) {
            console.log(util.format(
                'Successfully sent Proposal and received ProposalResponse: Status - %s, message - "%s"',
                proposalResponses[0].response.status, proposalResponses[0].response.message));

            // build up the request for the orderer to have the transaction committed
            var request = {
                proposalResponses: proposalResponses,
                proposal: proposal
            };

            // set the transaction listener and set a timeout of 30 sec
            // if the transaction did not get committed within the timeout period,
            // report a TIMEOUT status
            var transaction_id_string = tx_id.getTransactionID(); //Get the transaction ID string to be used by the event processing
            var promises = [];

            var sendPromise = channel.sendTransaction(request);
            promises.push(sendPromise); //we want the send transaction first, so that we know where to check status

            // get an eventhub once the fabric client has a user assigned. The user
            // is required bacause the event registration must be signed
            let event_hub = fabric_client.newEventHub();
            event_hub.setPeerAddr('grpc://localhost:7053');

            // using resolve the promise so that result status may be processed
            // under the then clause rather than having the catch clause process
            // the status
            let txPromise = new Promise((resolve, reject) => {
                let handle = setTimeout(() => {
                    event_hub.disconnect();
                    resolve({event_status : 'TIMEOUT'}); //we could use reject(new Error('Trnasaction did not complete within 30 seconds'));
                }, 3000);
                event_hub.connect();
                event_hub.registerTxEvent(transaction_id_string, (tx, code) => {
                    // this is the callback for transaction event status
                    // first some clean up of event listener
                    clearTimeout(handle);
                    event_hub.unregisterTxEvent(transaction_id_string);
                    event_hub.disconnect();

                    // now let the application know what happened
                    var return_status = {event_status : code, tx_id : transaction_id_string};
                    if (code !== 'VALID') {
                        console.error('The transaction was invalid, code = ' + code);
                        resolve(return_status); // we could use reject(new Error('Problem with the tranaction, event status ::'+code));
                    } else {
                        console.log('The transaction has been committed on peer ' + event_hub._ep._endpoint.addr);
                        resolve(return_status);
                    }
                }, (err) => {
                    //this is the callback if something goes wrong with the event registration or processing
                    reject(new Error('There was a problem with the eventhub ::'+err));
                });
            });
            promises.push(txPromise);

            return Promise.all(promises);
        } else {
            console.error('Failed to send Proposal or receive valid response. Response null or status is not 200. exiting...');
            throw new Error('Failed to send Proposal or receive valid response. Response null or status is not 200. exiting...');
        }
    }).then((results) => {
        console.log('Send transaction promise and event listener promise have completed');
        // check the results in the order the promises were added to the promise all list
        if (results && results[0] && results[0].status === 'SUCCESS') {
            console.log('Successfully sent transaction to the orderer.');
        } else {
            console.error('Failed to order the transaction. Error code: ' + results[0].status);
        }
    
        if(results && results[1] && results[1].event_status === 'VALID') {
            console.log('Successfully committed the change to the ledger by the peer');
        } else {
            console.log('Transaction failed to be committed to the ledger due to ::'+results[1].event_status);
        }
        res.status(200).send("Successfully removed transcript for id: " + id);

    }).catch(function(err) {
        res.status(500).json({error: err.toString()})
      })
});

const shell = require('shelljs');
app.post('/api/enrollUser', function (req, res) {
    var username = req.body.username;
    var password = req.body.password;
    var attr = [{name:"uid"}, {name:"hf.Type"}];

    var userJson;
    var signingIdentity;
    var privateKey;
    var publicKey;
    var name;
    
    var queryAttributes;
    // create the key value store as defined in the fabric-client/config/default.json 'key-value-store' setting
    Fabric_Client.newDefaultKeyValueStore({ path: store_path
    }).then((state_store) => {
        // assign the store to the fabric client
        fabric_client.setStateStore(state_store);
        var crypto_suite = Fabric_Client.newCryptoSuite();
        // use the same location for the state store (where the users' certificate are kept)
        // and the crypto store (where the users' keys are kept)
        var crypto_store = Fabric_Client.newCryptoKeyStore({path: store_path});
        crypto_suite.setCryptoKeyStore(crypto_store);
        fabric_client.setCryptoSuite(crypto_suite);
        var	tlsOptions = {
        trustedRoots: [],
        verify: false
        };
        // be sure to change the http to https when the CA is running TLS enabled
        fabric_ca_client = new Fabric_CA_Client('http://localhost:7054', tlsOptions , 'ca.acalink.com', crypto_suite);

        // first check to see if the admin is already enrolled
        return fabric_client.getUserContext(username, true);
    }).then((user_from_store) => {
        if (user_from_store && user_from_store.isEnrolled()) {
            //console.log('Successfully loaded from persistence');
        
            // Parse through user json
            userJson = JSON.parse(user_from_store.toString());
            signingIdentity = userJson.enrollment.signingIdentity;
            privateKey = signingIdentity.concat("-priv");
            publicKey = signingIdentity.concat("-pub");
            name = userJson.name;          
        }       

        // need to enroll it with CA server
        return fabric_ca_client.enroll({              
            enrollmentID: username,
            enrollmentSecret: password,
            attr_reqs: attr
        }).then((enrollment) => {
            // Remove private key, public key and certificate to log out
            shell.rm('-rf', './hfc-key-store/' + name);
            shell.rm('-rf', './hfc-key-store/' + privateKey);
            shell.rm('-rf', './hfc-key-store/' + publicKey);
            console.log('Successfully enrolled user : ' +username);
            return fabric_client.createUser(
                {username: username,
                    mspid: 'SheridancMSP',
                    cryptoContent: { privateKeyPEM: enrollment.key.toBytes(), signedCertPEM: enrollment.certificate },
                    skipPersistence: true
                });
        }).then((user) => {
            admin_user = user;
            return fabric_client.setUserContext(admin_user);
        }).catch((err) => {
            console.error('Failed to enroll and persist '+username+'. Error: ' + err.stack ? err.stack : err);
            throw new Error('Failed to enroll '+ username );
        });
    }).then(() => {
        return Fabric_Client.newDefaultKeyValueStore({ path: store_path
        }).then((state_store) => {
            // assign the store to the fabric client
            fabric_client.setStateStore(state_store);
            var crypto_suite = Fabric_Client.newCryptoSuite();
            // use the same location for the state store (where the users' certificate are kept)
            // and the crypto store (where the users' keys are kept)
            var crypto_store = Fabric_Client.newCryptoKeyStore({path: store_path});
            crypto_suite.setCryptoKeyStore(crypto_store);
            fabric_client.setCryptoSuite(crypto_suite);
    
            // get the enrolled user from persistence, this user will sign all requests
            return fabric_client.getUserContext(username, true);
        }).then((user_from_store) => {
            if (user_from_store && user_from_store.isEnrolled()) {
                console.log('Successfully loaded ' + username + ' from persistence');
                member_user = user_from_store;
            } else {
                throw new Error('Failed to get ' + username + ' run enrollUser.js');
            }

            const request = {
                //targets : --- letting this default to the peers assigned to the channel
                chaincodeId: 'acalink',
                fcn: 'getAttribute',
                args: []
            };
            http://localhost:9091/api/attribute/role
            // send the query proposal to the peer
            return channel.queryByChaincode(request);
        }).then((query_responses) => {
            console.log("Query has completed, checking results");
            // query_responses could have more than one  results if there multiple peers were used as targets
            if (query_responses && query_responses.length == 1) {
                if (query_responses[0] instanceof Error) {
                    console.error("error from query = ", query_responses[0]);
                } else {
                    console.log("Response is ", query_responses[0].toString());
                }
            } else {
                console.log("No payloads were returned from query");
            }
            
            queryAttributes = query_responses[0].toString();
            return queryAttributes;
        }).catch(function(err) {
            res.status(500).json({error: err.toString()})
          })
       
    }).then(() => {
        // Parsing through user
        var userJson = JSON.parse(admin_user.toString());
        // Parseing through attributes of user
        var attrJson = JSON.parse(queryAttributes);

        var json = {};
        var enrollmentAttribute = {};
        var identityAttribute ={};
        var attrs = {};

        enrollmentAttribute["signingIdentity"]=userJson.enrollment.signingIdentity;
        identityAttribute["certificate"]=userJson.enrollment.identity.certificate;
        attrs["role"]=attrJson.role;
        attrs["uid"]=attrJson.uid;

        json["name"]=userJson.name;
        json["mspid"]=userJson.mspid;
        json["enrollment"]=enrollmentAttribute;
        json["identity"]=identityAttribute;
        json["attributes"]=attrs;

        res.status(200).send(json);
    }).catch((err) => {
        console.error('Failed to enroll : ' + username + ' - ' + err);
        res.status(500).send({error: err.toString()});
    });
});

app.post('/api/logoutUser', function (req, res) {
    var username = req.body.username;
    //var password = req.body.password;
    
    // create the key value store as defined in the fabric-client/config/default.json 'key-value-store' setting
    Fabric_Client.newDefaultKeyValueStore({ path: store_path
    }).then((state_store) => {
        // assign the store to the fabric client
        fabric_client.setStateStore(state_store);
        var crypto_suite = Fabric_Client.newCryptoSuite();
        // use the same location for the state store (where the users' certificate are kept)
        // and the crypto store (where the users' keys are kept)
        var crypto_store = Fabric_Client.newCryptoKeyStore({path: store_path});
        crypto_suite.setCryptoKeyStore(crypto_store);
        fabric_client.setCryptoSuite(crypto_suite);
        var	tlsOptions = {
        trustedRoots: [],
        verify: false
        };
        // be sure to change the http to https when the CA is running TLS enabled
        fabric_ca_client = new Fabric_CA_Client('http://localhost:7054', tlsOptions , 'ca.acalink.com', crypto_suite);

        // first check to see if the admin is already enrolled
        return fabric_client.getUserContext(username, true);
    }).then((user_from_store) => {
        if (user_from_store && user_from_store.isEnrolled()) {
        console.log('Successfully loaded '+ username +' from persistence');
        admin_user = user_from_store;
        return null;
        } else {
            // need to enroll it with CA server
            return fabric_ca_client.enroll({
                enrollmentID: username,
                //enrollmentSecret: password
            }).then((enrollment) => {
                console.log('Successfully enrolled user : ' +username);
                return fabric_client.createUser(
                    {username: username,
                        mspid: 'SheridancMSP',
                        cryptoContent: { privateKeyPEM: enrollment.key.toBytes(), signedCertPEM: enrollment.certificate },
                        skipPersistence: true
                    });
            }).then((user) => {
                admin_user = user;
                return fabric_client.setUserContext(admin_user);
            }).catch((err) => {
                console.error('Failed to enroll and persist '+username+'. Error: ' + err.stack ? err.stack : err);
                throw new Error('Failed to logoff '+ username);
            });
        }
    }).then(() => {

        // Parse through user json
        var userJson = JSON.parse(admin_user.toString());
        var signingIdentity = userJson.enrollment.signingIdentity;
        var privateKey = signingIdentity.concat("-priv");
        var publicKey = signingIdentity.concat("-pub");
        var name = userJson.name;

        // Remove private key, public key and certificate to log out
        shell.rm('-rf', './hfc-key-store/' + name);
        shell.rm('-rf', './hfc-key-store/' + privateKey);
        shell.rm('-rf', './hfc-key-store/' + publicKey);
        
        res.status(200).send("Logout successful for: " + name);
     
    }).catch((err) => {
        console.error('Failed to logout : ' + username + ' - ' + err);
        res.status(500).json({error: err.toString()})
    });

});


app.get('/api/getAttribute', function (req, res) {
    
    var username = req.query.username
    // create the key value store as defined in the fabric-client/config/default.json 'key-value-store' setting
    Fabric_Client.newDefaultKeyValueStore({ path: store_path
    }).then((state_store) => {
        // assign the store to the fabric client
        fabric_client.setStateStore(state_store);
        var crypto_suite = Fabric_Client.newCryptoSuite();
        // use the same location for the state store (where the users' certificate are kept)
        // and the crypto store (where the users' keys are kept)
        var crypto_store = Fabric_Client.newCryptoKeyStore({path: store_path});
        crypto_suite.setCryptoKeyStore(crypto_store);
        fabric_client.setCryptoSuite(crypto_suite);

        // get the enrolled user from persistence, this user will sign all requests
        return fabric_client.getUserContext(username, true);
    }).then((user_from_store) => {
        if (user_from_store && user_from_store.isEnrolled()) {
            console.log('Successfully loaded ' + username + ' from persistence');
            member_user = user_from_store;
        } else {
            throw new Error('Failed to get ' + username + ' run enrollUser.js');
        }

        // queryCar chaincode function - requires 1 argument, ex: args: ['CAR4'],
        // queryAllCars chaincode function - requires no arguments , ex: args: [''],
        const request = {
            //targets : --- letting this default to the peers assigned to the channel
            chaincodeId: 'acalink',
            fcn: 'getAttribute',
            args: []
        };
        http://localhost:9091/api/attribute/role
        // send the query proposal to the peer
        return channel.queryByChaincode(request);
    }).then((query_responses) => {
        console.log("Query has completed, checking results");
        // query_responses could have more than one  results if there multiple peers were used as targets
        if (query_responses && query_responses.length == 1) {
            if (query_responses[0] instanceof Error) {
                console.error("error from query = ", query_responses[0]);
            } else {
                console.log("Response is ", query_responses[0].toString());
            }
        } else {
            console.log("No payloads were returned from query");
        }


        //var json = '{"role":"' + query_responses[0].toString() + '"}';
        
        //res.status(200).json({response: JSON.parse(query_responses[0].toString())});
        res.status(200).send(query_responses[0].toString());
    }).catch(function(err) {
        res.status(500).json({error: err.toString()})
      })
});

app.listen(PORT, HOST);
console.log(`Running on http://${HOST}:${PORT}`);
