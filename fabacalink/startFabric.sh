#!/bin/bash
#
# Copyright IBM Corp All Rights Reserved
#
# SPDX-License-Identifier: Apache-2.0
#
# Exit on first error
set -e

# don't rewrite paths for Windows Git Bash users
export MSYS_NO_PATHCONV=1
starttime=$(date +%s)
LANGUAGE=${1:-"golang"}
CC_SRC_PATH=github.com/fabacalink/go
if [ "$LANGUAGE" = "node" -o "$LANGUAGE" = "NODE" ]; then
	CC_SRC_PATH=/opt/gopath/src/github.com/fabcar/node
fi

# clean the keystore
rm -rf ./hfc-key-store

# launch network; create channel and join peer to channel
cd ../basic-network
./start.sh

# Now launch the CLI container in order to install, instantiate chaincode
# and prime the ledger with our 10 cars

#docker-compose -f docker-compose.yml -f docker-compose-couch.yaml up -d
docker-compose -f ./docker-compose.yml up -d cli couchdb

docker exec -e "CORE_PEER_LOCALMSPID=SheridancMSP" -e "CORE_PEER_MSPCONFIGPATH=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/peerOrganizations/sheridanc.acalink.com/users/Admin@sheridanc.acalink.com/msp" cli peer chaincode install -n acalink -v 1.0 -p "$CC_SRC_PATH" -l "$LANGUAGE"
docker exec -e "CORE_PEER_LOCALMSPID=SheridancMSP" -e "CORE_PEER_MSPCONFIGPATH=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/peerOrganizations/sheridanc.acalink.com/users/Admin@sheridanc.acalink.com/msp" cli peer chaincode instantiate -o orderer.acalink.com:7050 -C institutionch -n acalink -l "$LANGUAGE" -v 1.0 -c '{"Args":[""]}' -P "OR ('SheridancMSP.member','Org2MSP.member')"
sleep 10
docker exec -e "CORE_PEER_LOCALMSPID=SheridancMSP" -e "CORE_PEER_MSPCONFIGPATH=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/peerOrganizations/sheridanc.acalink.com/users/Admin@sheridanc.acalink.com/msp" cli peer chaincode invoke -o orderer.acalink.com:7050 -C institutionch -n acalink -c '{"function":"initLedger","Args":[""]}'

printf "\nTotal setup execution time : $(($(date +%s) - starttime)) secs ...\n\n\n"
printf "Start by installing required packages run 'npm install'\n"
printf "Then run 'node enrollAdmin.js', then 'node registerUser'\n\n"
printf "The 'node invokeTranscript.js' will fail until it has been updated with valid arguments\n\n"
printf "The 'node queryTranscirpt.js' may be run at anytime once the user has been registered\n\n"
printf "The 'node queryAllTranscirpt.js' may be run at anytime once the user has been registered\n\n"
printf "The 'node getTranscirptHistory.js' may be run at anytime once the user has been registered\n\n"