#!/bin/bash

# Print the usage message
function printHelp() {
  echo "Options: "
  echo "    To create a transcript:"
  echo "        transcript.sh create"
  echo "    To add a course on an existing transcript: "
  echo "        transcript.sh add"
  echo "    To query a transcript:"
  echo "        transcript.sh query <User> <TranscriptID>"              
  echo "    To query all transcripts:"
  echo "        transcript.sh queryAll <User>"
  echo "    To query all transcripts by Institution:"
  echo "        transcript.sh queryAllI <User> <Institution>"
  echo "    To delete a specific transcript"
  echo "        transcript.sh delete <User> <TranscriptID>"
  echo "    To get transcript history"
  echo "        transcript.sh history <User> <TranscriptID>"
}

MODE=$1

if  [ "$MODE" = "create" ]
then
    echo "Creating Transcript"
    node functions/transcript/invokeTranscript.js $2
elif [ "$MODE" = "add" ]
then
    echo "Adding Additional Course To Transcript"
    node functions/transcript/addCourseToTranscript.js $2
elif [ "$MODE" = "query" ]
then
    echo "Query Transcript"
    node functions/transcript/queryTranscript.js $2 $3
elif [ "$MODE" = "queryAll" ]
then
    echo "Querying All Transcripts"
    node functions/transcript/queryAllTranscript.js $2
elif [ "$MODE" = "queryAllI" ]
then
    echo "Querying All Transcripts By Institution"
    node functions/transcript/queryAllTranscriptsByInstitution.js $2 $3
elif [ "$MODE" = "delete" ]
then
    echo "Delete Transcript"
    node functions/transcript/deleteTranscript.js $2 $3
elif [ "$MODE" = "history" ]
then
    echo "Getting Transcript History"
    node functions/transcript/getTranscriptHistory.js $2 $3
else
  printHelp
  exit 1
fi
